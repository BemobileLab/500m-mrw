from calendar import timegm
import pytz
import datetime
import sys
import inspect
import six

def timestamp_from_datetime(dt):
    """
    Compute timestamp from a datetime object that could be timezone aware
    or unaware.
    """
    try:
        utc_dt = dt.astimezone(pytz.utc)
    except ValueError:
        utc_dt = dt.replace(tzinfo=pytz.utc)
    return timegm(utc_dt.timetuple())


def timestamp_from_date(date):
    epoch = datetime.date(1970, 1, 1)
    diff = date - epoch
    return diff.days * 24 * 3600 + diff.seconds


def reraise_as(new_exception_or_type):
    __traceback_hide__ = True  # NOQA

    e_type, e_value, e_traceback = sys.exc_info()

    if inspect.isclass(new_exception_or_type):
        new_type = new_exception_or_type
        new_exception = new_exception_or_type()
    else:
        new_type = type(new_exception_or_type)
        new_exception = new_exception_or_type

    new_exception.__cause__ = e_value

    try:
        six.reraise(new_type, new_exception, e_traceback)
    finally:
        del e_traceback


class AliasProperty(object):
    def __init__(self, name):
        self.name = name

    def __get__(self, instance, owner):
        return getattr(instance, self.name)

    def __set__(self, instance, value):
        return setattr(instance, self.name, value)


def add_camelcase_aliases(cls):
    for name in cls().__dict__.keys():
        if name[0] == '_':
            continue
        setattr(cls, name.title().replace('_', ''), AliasProperty(name))
    return cls


@add_camelcase_aliases
class Address(object):
    def __init__(self, street_type='CL', street=None, number_type=None, number=None, stairs=None,
                 floor=None, door=None, postal_code=None, city=None, province=None, country=None):
        self.street_type = street_type
        self.street = street
        self.number_type = number_type
        self.number = number
        self.stairs = stairs
        self.floor = floor
        self.door = door
        self.postal_code = postal_code
        self.city = city
        self.province = province
        self.country = country        

    def __str__(self):
        return 'Address: %s, %s , %s, %s, %s , %s,  %s, %s , %s, %s, %s' % \
               (self.street_type, self.street, self.number_type, self.number, self.stairs, self.floor, self.door,
                self.postal_code, self.city, self.province, self.country)

    def __eq__(self, other):
        if isinstance(other, Address):
            stat = ((self.street == other.street) and
                    (self.number == other.number) and
                    (self.stairs == other.stairs) and
                    (self.floor == other.floor) and
                    (self.door == other.door) and
                    (self.postal_code == other.postal_code) and
                    (self.city == other.city) and
                    (self.province == other.province) and
                    (self.country == other.country))
            return stat
        return False


@add_camelcase_aliases
class Person(object):
    def __init__(self, legal_name=None, name=None, last_name=None, phone_prefix=None, phone_number=None,
                 legal_number=None, email=None, language=None, address=None, time_from=None, time_to=None):
        self.legal_name = legal_name
        self.name = name
        self.last_name = last_name
        self.phone_prefix = phone_prefix
        self.phone_number = phone_number
        self.legal_number = legal_number
        self.email = email
        self.language = language
        self.address = address
        self.time_from = time_from
        self.time_to = time_to

    def __str__(self):
        return 'Person: %s, %s, %s, %s, %s, %s,  %s, %s, %s, %s, %s' % \
               (self.legal_name, self.name, self.last_name, self.phone_prefix, self.phone_number, self.legal_number,
                self.email, self.language, self.address, self.time_from, self.time_to)

    def __eq__(self, other):
        if isinstance(other, Person):
            stat = ((self.name == other.name) and
                    (self.last_name == other.last_name) and
                    (self.email == other.email))
            return stat
        return False
