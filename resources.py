from base import BaseApiModel
from .fields import *
from .query import InsertQuery, SelectQuery, BaseQuery


class Shipment(BaseApiModel):
    pickup_datetime = DateTimeField(api_name='Fecha', api_value_callback=lambda x: x.strftime("%d/%m/%Y"))
    number_of_packages = IntegerField(api_name='NumeroBultos', default=1)
    weight = IntegerField(api_name='Peso', default=2)
    value = FloatField(api_name='ValorDeclarado')
    observations = CharField()
    receiver = ReceiverField(api_type='DatosDestinatarioRequest', api_name='DatosEntrega')
    sender = SenderField(api_type='DatosRemitenteRequest', api_name='DatosRecogida')
    declared_value = FloatField()
    reference = CharField(api_name='Referencia')
    locator = PrimaryKeyField(api_name='NumeroEnvio')
    history = ListField()
    fee = FloatField()

    @property
    def status(self):
        if self.history:
            if filter(lambda x: x.lower() == 'entregado', self.history):
                return 'Delivered'
            if filter(lambda x: x.lower() == 'recogido', self.history):
                return 'Picked'
        if self.locator:
            return 'Created'
        return None

    class Meta:
        verbose_name = 'shipment'
        verbose_name_plural = 'shipments'
        url = {
            InsertQuery.identifier: {
                'ENDPOINT': '/MRWEnvio.asmx',
                'METHOD': 'TransmEnvio',
                'REQUEST_BUILD_CALLBACK': '_build_insert_request'
            },
            SelectQuery.identifier: {
                'ENDPOINT': '/TrackingService.svc',
                'METHOD': 'GetEnvios',
                'REQUEST_BUILD_CALLBACK': '_build_select_request'
            }
        }

    def save(self, cls=None, idempotency_key=None):
        self.clean()
        field_dict = dict(self._data)
        field_dict.update(self.get_field_dict())
        field_dict.pop(self._meta.pk_name)
        all_fields = self._meta.fields

        for k, v in all_fields.items():
            if v.required is True and field_dict[v.name] is None:
                raise ValueError('Missing mandatory field: ' + v.name)

        insert = self.insert(idempotency_key=idempotency_key, **field_dict)
        result = insert.execute()

        for key, value in result.items():
            setattr(self, key, value)

        return result

    def clean(self):
        self.sender.time_from = self.pickup_datetime
        self.sender.time_to = self.pickup_datetime + datetime.timedelta(hours=2)

    @classmethod
    def _build_insert_request(cls, type_factory, data):
        service = {
            cls.pickup_datetime.api_name: data[cls.pickup_datetime.api_name],
            cls.reference.api_name: data[cls.reference.api_name],
            cls.number_of_packages.api_name: data[cls.number_of_packages.api_name],
            cls.value.api_name: int(data[cls.value.api_name]),
            cls.weight.api_name: data[cls.weight.api_name],
            'CodigoServicio': '0205',
            'TipoMercancia': 'MCV',
            'Notificaciones': type_factory.ArrayOfNotificacionRequest(
                cls._build_notifications_request(type_factory, data['DatosEntrega'].pop('Email'), data['DatosEntrega']['Telefono']) +
                cls._build_notifications_request(type_factory, data['DatosRecogida'].pop('Email'), data['DatosRecogida']['Telefono'])),
            'SeguroOpcional': type_factory.SeguroOpcionalRequest('1', str(int(data[cls.value.api_name])))
        }
        request = {'DatosServicio': type_factory.DatosServicioRequest(**service)}

        for k, v in data.iteritems():
            if isinstance(v, dict):
                api_type = v.pop('_api_type')
                request[k] = getattr(type_factory, api_type)(**BaseQuery._build_wsdl_object(type_factory, v))

        return [type_factory.TransmEnvioRequest(**request)]

    @classmethod
    def _build_notifications_request(cls, type_factory, email, phone_number):
        return [
            # type_factory.NotificacionRequest('1', '1', email),
            # type_factory.NotificacionRequest('1', '2', email),
            # type_factory.NotificacionRequest('1', '3', email),
            # type_factory.NotificacionRequest('1', '4', email),
            # type_factory.NotificacionRequest('1', '5', email),
            # type_factory.NotificacionRequest('2', '1', phone_number),
            type_factory.NotificacionRequest('2', '2', phone_number),
            # type_factory.NotificacionRequest('2', '3', phone_number),
            # type_factory.NotificacionRequest('2', '4', phone_number),
            # type_factory.NotificacionRequest('2', '5', phone_number),
        ]

    @classmethod
    def _build_select_request(cls, data):
        from . import Config
        request = {
            'login': Config.auth['UserName'],
            'pass': Config.auth['Password'],
            'codigoIdioma': 3082,
            'tipoFiltro': '0',
            'valorFiltroDesde': data,
            'tipoInformacion': '0',
            'codigoFranquicia': Config.auth['CodigoFranquicia'],
            'codigoAbonado': Config.auth['CodigoAbonado']
        }
        return request


