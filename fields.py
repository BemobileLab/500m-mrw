import json
import re
import time
import datetime
import six

from .utils import timestamp_from_datetime, timestamp_from_date, Address, Person

import sys


class FieldDescriptor(object):
    def __init__(self, field):
        self.field = field
        self.att_name = self.field.name

    def __get__(self, instance, instance_type=None):
        if instance is not None:
            return instance._data.get(self.att_name)

        return self.field

    def __set__(self, instance, value):
        instance._data[self.att_name] = value


class Field(object):
    default = None
    _field_counter = 0
    _order = 0

    def get_attributes(self):
        return {}

    def __init__(self, null=False, api_name=None,
                 help_text=None, api_value_callback=None,
                 choices=None, default=None, api_type=None,
                 python_value_callback=None, *args, **kwargs):
        self.null = null
        self.attributes = self.get_attributes()
        self.default = kwargs.get('default', None)
        self.api_name = api_name
        self.api_type = api_type
        self.api_value_callback = api_value_callback
        self.python_value_callback = python_value_callback
        self.help_text = help_text
        self.required = kwargs.get('required', False)
        self.choices = choices
        self.default = default

        self.attributes.update(kwargs)

        self._order = Field._field_counter

    def add_to_class(self, klass, name):
        self.name = name
        self.model = klass
        self.api_name = self.api_name or re.sub('_+', ' ', name).title()

        klass._meta.fields[self.name] = self

        setattr(klass, name, FieldDescriptor(self))

    def null_wrapper(self, value, default=None):
        if (self.null and not value) or not default:
            return value
        return value or default

    def api_value(self, value):
        if self.api_value_callback:
            value = self.api_value_callback(value)
        return value

    def python_value(self, value):
        if self.python_value_callback:
            value = self.python_value_callback(value)
        return value


class CharField(Field):
    def python_value(self, value):
        if self.python_value_callback:
            value = self.python_value_callback(value)

        return value

    def api_value(self, value):
        if sys.version_info > (3, 0) and isinstance(value, six.binary_type):
            return value.decode('utf-8')

        return value


class DateTimeField(Field):
    def python_value(self, value):
        value = super(DateTimeField, self).python_value(value)

        if isinstance(value, six.string_types):
            value = value.rsplit('.', 1)[0]
            value = datetime(*time.strptime(value, '%Y-%m-%d %H:%M:%S')[:6])

        if isinstance(value, six.integer_types):
            value = datetime.datetime.utcfromtimestamp(value)

        return value

    def api_value(self, value):
        value = super(DateTimeField, self).api_value(value)

        if isinstance(value, datetime.datetime):
            value = timestamp_from_datetime(value)

        return value


class DateField(Field):
    def python_value(self, value):
        value = super(DateField, self).python_value(value)

        if isinstance(value, six.string_types):
            value = datetime.datetime.strptime(value, '%Y-%m-%d').date()

        if isinstance(value, six.integer_types):
            value = datetime.datetime.utcfromtimestamp(value).date()

        return value

    def api_value(self, value):
        value = super(DateField, self).api_value(value)

        if isinstance(value, datetime.date):
            value = timestamp_from_date(value)

        return value


class IntegerField(Field):
    def api_value(self, value):
        return self.null_wrapper(super(IntegerField, self).api_value(value), 0)

    def python_value(self, value):
        if value is not None:
            return int(super(IntegerField, self).python_value(value))


class FloatField(Field):
    def api_value(self, value):
        return self.null_wrapper(super(FloatField, self).api_value(value), 0.0)

    def python_value(self, value):
        if value is not None:
            return float(super(FloatField, self).python_value(value))


class DictField(Field):
    def api_value(self, value):
        return json.dumps(value)

    def python_value(self, value):
        if value is not None and isinstance(value, str):
            return json.loads(value)
        elif isinstance(value, dict):
            return value


class PrimaryKeyField(CharField):
    pass


class ListField(Field):
    pass


class BooleanField(IntegerField):
    def api_value(self, value):
        value = super(BooleanField, self).api_value(value)

        if value:
            return 1
        return 0

    def python_value(self, value):
        value = super(BooleanField, self).python_value(value)

        return bool(value)


class EmailField(CharField):
    pass


class AddressField(Field):
    def python_value(self, value):
        if value is not None:
            stairs, floor, door = self._deserialize_remaining(value)
            return Address(street_type=value['CodigoTipoVia'], street=value['Via'], stairs=stairs, floor=floor,
                           door=door, number=value['Numero'], city=value['Poblacion'], province=value['Estado'],
                           postal_code=value['CodigoPostal'], country=value['CodigoPais'])

        return value

    def api_value(self, value):
        value = super(AddressField, self).api_value(value)

        if isinstance(value, Address):
            value = {
                'CodigoTipoVia': value.street_type,
                'Via': value.street,
                'Numero': value.number,
                'Resto': self._serialize_remaining(value),
                'Poblacion': value.city,
                'Estado': value.province,
                'CodigoPostal': self._serialize_postal_code(value),
                'CodigoPais': value.country
            }

            if self.api_type:
                value['_api_type'] = self.api_type

        return value

    def _serialize_postal_code(self, value):
        postal_code = value.postal_code
        if value.country.lower() == 'pt':
            return postal_code[:4] if len(postal_code) > 4 else postal_code
        else:
            return postal_code

    def _serialize_remaining(self, value):
        remaining = ''
        if value.stairs:
            remaining += ', ' + value.stairs
        if value.floor:
            remaining += ', ' + value.floor
        if value.door:
            remaining += ', ' + value.door
        return remaining

    def _deserialize_remaining(self, value):
        if value['Resto']:
            stairs, floor, door = value['Resto'].split(', ')
            stairs = '' if not stairs else stairs
            floor = '' if not floor else floor
            door = '' if not door else door
            return stairs, floor, door
        return [None] * 3


class PersonField(Field):
    def python_value(self, value):
        if value is not None:
            name, last_name = self._deserialize_name(value)
            time_from, time_to = self._deserialize_time(value)
            return Person(name=name, last_name=last_name, phone_prefix='+34', phone_number=value['Telefono'],
                          legal_number=value['Nif'], address=AddressField().python_value(value['Direccion']),
                          time_from=time_from, time_to=time_to)

        return value

    def api_value(self, value):
        value = super(PersonField, self).api_value(value)

        if isinstance(value, Person):
            value = {
                'Direccion': AddressField(api_type='DireccionRequest').api_value(value.address),
                'Nombre': self._serialize_name(value),
                'Telefono': value.phone_number.replace(' ', ''),
                'Email': value.email,
                'Nif': value.legal_number,
                'Contacto': self._serialize_name(value),
                'Horario': self._serialize_time(value)
            }

            if self.api_type:
                value['_api_type'] = self.api_type

        return value

    def _serialize_name(self, value):
        return '%s %s' % (value.name, value.last_name)

    def _deserialize_name(self, value):
        if value['Nombre']:
            names = value['Nombre'].split(' ')
            if len(names) > 1:
                return names[0], ' '.join(names[1:])
            return names[0], ''
        return [None] * 2

    def _serialize_time(self, value):
        if value.time_from and value.time_to:
            return {'Rangos': [{
                'HorarioRangoRequest': {
                    'Desde': value.time_from.strftime("%H:%M"),
                    'Hasta': value.time_to.strftime("%H:%M")
                }
            }]}
        return None

    def _deserialize_time(self, value):
        if value.get('Horario') and value['Horario'].get('Rangos') and value['Horario']['Rangos'].get(
                'HorarioRangoRequest'):
            return value['Horario']['Rangos']['HorarioRangoRequest']['Desde'], \
                   value['Horario']['Rangos']['HorarioRangoRequest']['Hasta']
        return [None] * 2


class ReceiverField(PersonField):
    def api_value(self, person):
        value = super(ReceiverField, self).api_value(person)
        if value and isinstance(person, Person):
            value['ALaAtencionDe'] = self._serialize_name(person)
        return value


class SenderField(PersonField):
    pass


class TimeField(Field):
    def python_value(self, value):
        value = super(TimeField, self).python_value(value)
        if value:
            datetime.datetime.strptime(value, "%H:%M")
        return value

    def api_value(self, value):
        value = super(TimeField, self).api_value(value)
        if value:
            datetime.datetime.strptime(value, "%H:%M")
        return value
